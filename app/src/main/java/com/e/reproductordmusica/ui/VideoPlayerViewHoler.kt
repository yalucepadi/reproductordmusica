package com.e.reproductordmusica.ui

import android.R
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager


class VideoPlayerViewHoler(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var media_container: FrameLayout? = null
    var title: TextView? = null
    var thumbnail: ImageView? = null
    var volumeControl:ImageView? = null
    var progressBar: ProgressBar? = null
    var parent: View? = null
    var requestManager: RequestManager? = null

    fun VideoPlayerViewHolder(itemView: View) {

        parent = itemView


    }



}