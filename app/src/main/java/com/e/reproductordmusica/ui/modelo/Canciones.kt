package com.e.reproductordmusica.ui.modelo

import kotlin.time.ExperimentalTime

data class Canciones @ExperimentalTime constructor(val nombre:String, val duration: Int)
