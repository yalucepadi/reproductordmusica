package com.e.reproductordmusica.models

class MediaObject(s: String, s1: String, s2: String, s3: String) {
    private var title: String? = null
    private var media_url: String? = null
    private var thumbnail: String? = null
    private var description: String? = null


    fun MediaObject(title: String?, media_url: String?, thumbnail: String?, description: String?) {
        this.title = title
        this.media_url = media_url
        this.thumbnail = thumbnail
        this.description = description
    }

    fun MediaObject() {}


    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getMedia_url(): String? {
        return media_url
    }

    fun setMedia_url(media_url: String?) {
        this.media_url = media_url
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }

    fun getThumbnail(): String? {
        return thumbnail
    }

    fun setThumbnail(thumbnail: String?) {
        this.thumbnail = thumbnail
    }



}