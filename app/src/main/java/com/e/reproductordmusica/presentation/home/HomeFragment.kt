package com.e.reproductordmusica.presentation.home


import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.e.reproductordmusica.R
import com.e.reproductordmusica.databinding.FragmentHomeBinding
import com.e.reproductordmusica.models.Samples
import com.e.reproductordmusica.presentation.home.Reproduccion.Companion.player
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Log
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class HomeFragment : Fragment() {
    private lateinit var tvNombre: TextView
    private lateinit var ivPerfil: ImageView
    private lateinit var viewModel: HomeViewModel
    private var _binding:FragmentHomeBinding? = null

    companion object {
        fun newInstance() = Fragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false);
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view;
    }
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup(view)
        setupViewModel()
        setUpReproducion()
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            showToast1("not allowed")
        }
    }

    private fun showToast1(text: CharSequence, duration: Int = Toast.LENGTH_LONG) {
       // Toast.makeText(this, text, duration).show()
        Toast.makeText(requireContext(), text,duration).show()

    }

    private fun setup(view: View) {
        tvNombre = view.findViewById(R.id.tvNombre)
        ivPerfil = view.findViewById(R.id.ivPerfil)
        binding.btnLogOut.setOnClickListener {
         signOut()
            val bundle = Bundle()
            bundle.putString("userID", "3ad50gfbcdbu")
            findNavController().navigate(R.id.action_homeFragment_to_loginFragment, bundle)

        }


    }



    private fun setUpReproducion(){
                    onStart()
                    onStop()
                    onDestroy()




    }



    fun Context.showToast(text: CharSequence, duration: Int = Toast.LENGTH_LONG){
        Toast.makeText(this, text, duration).show()

    }
    override fun onStart() {
        super.onStart()
        val re:Reproduccion?=null

        Reproduccion.player = ExoPlayerFactory.newSimpleInstance(
            requireContext(),
            DefaultTrackSelector()
        )
        binding.playerView.player = Reproduccion.player
        val dataSourceFactory = DefaultDataSourceFactory(
            requireContext(),
            Util.getUserAgent(requireContext(), getString(R.string.app_name))
        )
        val playerInfo = Util.getUserAgent(requireContext(), "ReproductorDmusica")
        val dataSourceFactory1 = DefaultDataSourceFactory(
            context, playerInfo
        )
        val mediaSource1: MediaSource = ExtractorMediaSource.Factory(dataSourceFactory)
            .setExtractorsFactory(DefaultExtractorsFactory())
            .createMediaSource(Samples.Mp3_uri)
        Reproduccion.player?.prepare(mediaSource1)
        Reproduccion.player?.playWhenReady = true
    }

    override fun onStop() {
        super.onStop()

        binding.playerView.player = null
        player?.release()
        player = null
    }

    override fun onDestroy() {
        super.onDestroy()

        //adsLoader.release()
    }

    fun getBitmapFromURL(src: String?): Bitmap? {
        return try {
            if (src != null) {
                Log.e("src", src)
            }

            val url = URL(src)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input: InputStream = connection.getInputStream()
            val myBitmap = BitmapFactory.decodeStream(input)
            Log.e("Bitmap", "returned")
            myBitmap
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e("Exception", e.toString())
            null
        }
    }

    private fun signOut() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso);
        mGoogleSignInClient.signOut()

    }

    private fun setupViewModel() {

        val acct = GoogleSignIn.getLastSignedInAccount(activity)
        if (acct != null) {
            val personName = acct.displayName
            val personGivenName = acct.givenName
            val personFamilyName = acct.familyName
            val personEmail = acct.email
            val personId = acct.id
            val personPhoto = acct.photoUrl
        }

        val userID = arguments?.getString("userID")
        if (userID!=null) {
            //Every Fragment or Activity has their own ViewModelStore and that's why they are a ViewModelStoreOwner
            val vmFactory = HomeVMFactory(userID)
            viewModel = ViewModelProvider(this, vmFactory).get(HomeViewModel::class.java)
            with(viewModel) {
                val acct = GoogleSignIn.getLastSignedInAccount(activity)
                if(acct!=null){

                viewLD.observe(viewLifecycleOwner) {
                    tvNombre.text = acct?.displayName
                   if(acct.photoUrl != null){
                    ivPerfil.setImageBitmap(getBitmapFromURL((acct?.photoUrl).toString()))
                    //ivPerfil.setImageURI(acct?.photoUrl)
                   }
                    else{
                       ivPerfil.setImageBitmap(getBitmapFromURL("https://media.metrolatam.com/2020/05/02/template8-f93124241dda526dd74c9dc67a452926-1200x800.jpg"))
                    }


                }

                }
                else{

                    viewLD.observe(viewLifecycleOwner) {
                        tvNombre.text =it.name
                        ivPerfil.setImageResource(it.picture)}
                }
            }
        } else {
            //gestionar error
        }
    }



}


