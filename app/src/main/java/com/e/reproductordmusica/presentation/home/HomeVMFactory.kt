package com.e.reproductordmusica.presentation.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.e.reproductordmusica.models.UsersRepository

class HomeVMFactory(private val userID: String): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(userID, UsersRepository()) as T
    }
}
