package com.e.reproductordmusica.presentation.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.e.reproductordmusica.common.SingleLiveEvent

class LoginViewModel : ViewModel() {
    private val _loginResultSLE = SingleLiveEvent<Boolean>()
    val loginResultLD: LiveData<Boolean> = _loginResultSLE

    fun login(username: String, password: String) {
        if (username=="yalucepadi" && password=="123") {
            _loginResultSLE.value = true
        } else {
            _loginResultSLE.value = false
        }
    }

}