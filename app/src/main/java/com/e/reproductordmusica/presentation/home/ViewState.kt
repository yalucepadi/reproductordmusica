package com.e.reproductordmusica.presentation.home

import androidx.annotation.DrawableRes

internal class ViewState(val name: String,
                         @DrawableRes val picture: Int)