package com.e.reproductordmusica.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.e.reproductordmusica.R
import com.e.reproductordmusica.models.User
import com.e.reproductordmusica.models.UsersRepository

internal class HomeViewModel(val userID: String, private val repository: UsersRepository) : ViewModel() {
    private val viewMLD = MutableLiveData<ViewState>()
    val viewLD: LiveData<ViewState> = viewMLD
    init {
        loadUser()
    }
    private fun loadUser() {
        val user: User = repository.fetchUserFromBackend(userID)
        viewMLD.value = ViewState(user.name, R.drawable.gatito)
    }

}